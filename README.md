[**Présentation**](https://misha.gitpages.huma-num.fr/phun/metadata/) donnée le 6 juillet 2023 dans le cadre de [l'école d'été 2023 du consortium DISTAM](https://distam.hypotheses.org/5509), et le 3 octobre 2024 dans le cadre des [ateliers Digit-Hum](https://digithum.huma-num.fr/).

Pour voir la présentation, [**cliquez ici**](https://misha.gitpages.huma-num.fr/phun/metadata/).
